import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:angular2/angular2.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:users_dartlang/src/models/user_models.dart';

@Injectable()
class UserXMLHttpRequestStorage extends MockClient {
  static final _initialUsers = [
    {
      'id': 1,
      'name': 'Tom',
      'email': 'tom@mailexample.com'
    },
    {
      'id': 2,
      'name': 'Jary',
      'email': 'jary@mailexample.com'
    },
    {
      'id': 3,
      'name': 'Pol',
      'email': 'pol@mmailexampleail.com'
    },
    {
      'id': 4,
      'name': 'Frank',
      'email': 'frank@mailexample.com'
    },
    {
      'id': 5,
      'name': 'Bill',
      'email': 'bill@mailexample.com'
    }
  ];
  static List<User> _usersDb;
  static int _nextId;
  static Future<Response> _handler(Request request) async {
    if (_usersDb == null) resetDb();
    var data;
    switch (request.method) {
      case 'GET':
        final id = int.parse(request.url.pathSegments.last, onError: (_) => null);
        if (id != null) {
          data = _usersDb
              .firstWhere((user) => user.id == id); // throws if no match
        } else {
          data = _usersDb.toList();
        }
        break;
      case 'POST':
        var name = JSON.decode(request.body)['name'];
        var email = JSON.decode(request.body)['email'];
        var newUser = new User({'id': _nextId++, 'name': name, 'email': email});
        _usersDb.add(newUser);
        data = newUser;
        break;
      case 'PUT':
        var userChanges = new User.fromJson(JSON.decode(request.body));
        var targetUser = _usersDb.firstWhere((h) => h.id == userChanges.id);
        targetUser.name = userChanges.name;
        data = targetUser;
        break;
      case 'DELETE':
        final id = int.parse(request.url.pathSegments.last, onError: (_) => null);
        _usersDb.removeWhere((user) => user.id == id);
        // No data, so leave it as null.
        break;
      default:
        throw 'Unimplemented HTTP method ${request.method}';
    }
    return new Response(JSON.encode({'data': data}), 200,
        headers: {'content-type': 'application/json'});
  }
  static resetDb() {
    _usersDb = _initialUsers.map((json) => new User.fromJson(json)).toList();
    _nextId = _usersDb.map((user) => user.id).fold(0, max) + 1;
  }
  static String lookUpName(int id) =>
      _usersDb.firstWhere((user) => user.id == id, orElse: null)?.name;
  UserXMLHttpRequestStorage() : super(_handler);
}





