import 'dart:async';
import 'dart:convert';
import 'package:angular2/core.dart';

import '../models/user_models.dart';
import 'package:http/http.dart';

@Injectable()
class UserService {
  final Client _http;

  static final userUrl = 'api/users/';
  static final _headers = {'Content-Type': 'application/json'};

  UserService(this._http);

  Future<List<User>> getUserList() async {
    try {
      final response = await _http.get(userUrl);
      final users = _extractData(response)
          .map((value) => new User.fromJson(value))
          .toList();
      return users;
    } catch (e) {
      throw _handleError(e);
    }
  }

  Future<User> getUser(int id) async {
    try {
      final response = await _http.get('$userUrl/$id');
      return new User.fromJson(_extractData(response));
    } catch (e) {
      throw _handleError(e);
    }
  }

  Future<User> save(user) async {
    try {
      final response = await _http.post(userUrl,
          headers: _headers, body: JSON.encode(user));
      return new User.fromJson(_extractData(response));
    } catch (e) {
      throw _handleError(e);
    }
  }

  Future<Null> remove(int id) async {
    try {
      final url = '$userUrl/$id';
      await _http.delete(url, headers: _headers);
    } catch (e) {
      throw _handleError(e);
    }
  }

  dynamic _extractData(Response resp) => JSON.decode(resp.body)['data'];

  Exception _handleError(dynamic e) {
    print(e); // for demo purposes only
    return new Exception('Server error; cause: $e');
  }
}