import 'dart:async';
import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';

import '../models/user_models.dart';
import 'user_list_service.dart';

@Component(
  selector: 'users-list',
  templateUrl: 'users_list_component.html',
  directives: const [
    COMMON_DIRECTIVES,
    ROUTER_DIRECTIVES
  ],
  pipes: const [COMMON_PIPES],
)
class UserListComponent implements OnInit {
  final UserService _userListService;

  final title = 'Users List';
  List<User> users;

  var user;

  UserListComponent(this._userListService) {
  }

  final valid = true;

  ngOnInit() {
    this.initUsers();
  }

  Future<Null> initUsers() async {
    this.users = await this._userListService.getUserList();
  }

  Map<String, bool> controlStateClasses(NgControl control) => {
    'ng-dirty': control.dirty ?? false,
    'ng-pristine': control.pristine ?? false,
    'ng-touched': control.touched ?? false,
    'ng-untouched': control.untouched ?? false,
    'ng-valid': control.valid ?? false,
    'ng-invalid': control.valid == false
  };

  Future<Null> save(form) async {
    if(form.valid) {
      user = {
        'name': form.value['name'].trim(),
        'email': form.value['email'].trim()
      };

      this.users.add(await this._userListService.save(user));

      form.controls.forEach((k,v) {
        form.controls[k].updateValue('');
        form.controls[k].setErrors(null);
      });
    }
  }

  Future<Null> remove(User user) async {
    await this._userListService.remove(user.id);
    this.users.remove(user);
  }
}