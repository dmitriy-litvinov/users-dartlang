import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';

import '../models/user_models.dart';
import 'package:users_dartlang/src/users_list/user_list_service.dart';

@Component(
  selector: 'user',
  templateUrl: 'user_component.html',
  directives: const [CORE_DIRECTIVES],
  pipes: const [COMMON_PIPES],
)
class UserComponent implements OnInit {
  final UserService _userListService;
  final RouteParams _routeParams;

  final title = 'Users details';
  User user;

  UserComponent(this._userListService, this._routeParams) {
  }

  ngOnInit()  {
    var _id = _routeParams.get('id');
    var id = int.parse(_id ?? '', onError: (_) => null);
    if (id != null) this.initUser(3);
  }

  initUser(int id) async {
    this.user = await this._userListService.getUser(id);
  }

}