class User {
  int id;
  String name;
  String email;

  User(user) {
    this.id = user['id'];
    this.name = user['name'];
    this.email = user['email'];
  }

  factory User.fromJson(Map<String, dynamic> user) =>
      new User({'id': _toInt(user['id']), 'name': user['name'], 'email': user['email']});

  Map toJson() => {'id': id, 'name': name, 'email': email};
}

int _toInt(id) => id is int ? id : int.parse(id);