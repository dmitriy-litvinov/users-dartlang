import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';

import 'package:users_dartlang/src/users_list/user_list_service.dart';
import 'src/users_list/user_list_component.dart';
import 'src/user/user_component.dart';

@Component(
  selector: 'my-app',
  styleUrls: const ['app_component.css'],
  templateUrl: 'app_component.html',
  directives: const [
    ROUTER_DIRECTIVES,
    UserListComponent,
    UserComponent
  ],
  providers: const [
    ROUTER_PROVIDERS,
    UserService
  ],
)
@RouteConfig(const [
  const Route(path: '/users', name: 'Users', component: UserListComponent, useAsDefault: true),
  const Route(path: '/user/:id', name: 'User', component: UserComponent),
])
class AppComponent {
}