import 'package:angular2/angular2.dart';
import 'package:angular2/platform/browser.dart';
import 'package:http/http.dart';

import 'package:users_dartlang/app_component.dart';
import 'package:users_dartlang/src/users_list/UserXMLHttpRequestStorage_service.dart';

void main() {
  bootstrap(AppComponent, [provide(Client, useClass: UserXMLHttpRequestStorage)]
    // Using a real back end?
    // Import browser_client.dart and change the above to:
    // [provide(Client, useFactory: () => new BrowserClient(), deps: [])]
  );
}